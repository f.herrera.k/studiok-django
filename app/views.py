from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'app/home.html')

def produtos(request):
    return render(request, 'app/productos.html')    

def nosotros(request):
    return render(request, 'app/nosotros.html') 

def contacto(request):
    return render(request, 'app/contacto.html') 